# Implicit Relational Assessment Procedure
This is a website that was created for Tom Geist. It utilizes React for the front end, Django for the backend, sqlite3
for the database and GraphQL for the APIs. It features a backend that can create customizable studies and store their 
trial results for later export as well as a frontend that guides users through the study. It is currently hosted on UVM Silk
and embedded in a qualtrics survey.


# Managing the Site
In order to most effectively manage the site, make sure you have SSH access to the uvm silk server. Here is a link to
UVM documentation on [logging in and uploading files to silk](https://silk.uvm.edu/manual/logging-in/)

Once you follow the instructions to log in without a password, the following command will restart the backend:
`pkill -u irap -f wsgi-loader`

## SECRET_KEY
A file named `secret_key.txt` must be stored in the parent directory of where the site is stored. i.e. for deployment, 
assuming that the files are stored at `~/django-root/`, there must be a file at `~/secret_key.txt` with a secret key.

## Deployment
Ensure `uri: "https://irap.w3.uvm.edu/backend/graphql/""` is the only address not commented out in `index.js`

`deploy.sh` in the root of the project can be used to deploy the site. This uploads the relevant files to `~/django-root`, 
as well as the static files to `~/www-root/static`. 
If the front end changes, you must manually run `npm run build` in the frontend directory.
If the back end changes, you must manually run `python manage.py collectstatic` in the backend directory.
If you have altered the tables for the database you must manually run `python manage.py makemigrations` and `python manage.py migrate`
once you are ready to apply those migrations. Read more about that [here](https://docs.djangoproject.com/en/3.2/topics/migrations/)


Important: If you choose to do this manually, do not upload db.sqlite3 or the media folder as this will overwrite the 
server’s database and media.

A `.htaccess` file is included in the project and should be uploaded along with the rest of the project. 
This is set up to work with Phusion Passenger on UVM’s silk v2 server. It ensures that https protocol is used and 
specifies to Passenger how to start the application. It assumes that the Django project is located in ~/django-root, and
that it is being served from the `~/www-root folder`. If there are problems with the server starting the application, 
edit this file, and change PassengerAppEnv production to PassengerAppEnv development. 
This will give a traceback if the application cannot be started.

# Hosting the site on a local machine

The following conditions must be satisfied for the site to run:

## Backend
Must be in backend directory for the following steps:
### Virtual environment

To set up the python environment, first install the tool `virtualenv`:

`pip install --user virtualenv`

Then create and activate the new virtual environment:

`virtualenv venv`

`source venv/bin/activate`

and install the required packages:

`pip install -r requirements`

#### Development environment
To start server after virtual environment activation:

`python manage.py runserver`


## Front end

Must be in frontend directory for the following steps:

Run `npm install` to install all dependencies

Ensure `uri: "http://127.0.0.1:8000/backend/graphql/"` is the only address not commented out in `index.js`

Run `npm run build` and `npm run start` to start the frontend on your local machine. It will only work if the backend is
already up and running. First go to `http://localhost:8080/app/` or whichever port your machine puts you on and click the link to the smoking study.
Currently there isn't a good way to not have to go back to this weird page every time the project re-renders on the local machine.


# Front end structure

## `index.js`
This is the main app file, contains the apollo provider wrapper and the main IRAP study. It also gets the participant ID
passed in through the query string in the URL. It than routes everything to the right study. renders `Irap.jsx`

## `Irap.jsx`
Contains the Query `GET_STUDY` from graphql and pulls all the study attributes into the frontend. Renders `Study.jsx`

## `Study.jsx`
Main force of the structure of the website. Contains logic to determine what "screen" is showing. `getLatency` and `getAccuracy`
methods can be found here. Renders `Message.jsx`, `Block.jsx` and `SendData.jsx`

## `Message.jsx`
Displays whatever message needs to be displayed and contains the keyboard event handler to move onto the next "page"

## `Block.jsx`
Contains the trialset and all information about it. Retrieves keystroke information from `Trial.jsx`, which it renders.
Also contains the logic to save the trial and determine a wrong answer.

## `Trial.jsx`
Handles recording keystrokes for each key-value pair in the study.

## `SendData.jsx`
Renders a submit button that when pressed sends all the data to the backend in a graphql mutation and erases participant data to prevent
it being saved more than once.
