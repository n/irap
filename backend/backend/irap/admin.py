from django.contrib import admin

from .models import Study, Label, Target, Response, Participant, Trial
# Register your models here.

import csv
from django.http import HttpResponse


class ExportCsvMixin:
    def export_to_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])

        return response

    export_to_csv.short_description = "Export Selected"


class TrialAdmin(admin.ModelAdmin, ExportCsvMixin):
    actions = ["export_to_csv"]


admin.site.register(Study)
admin.site.register(Label)
admin.site.register(Target)
admin.site.register(Response)
admin.site.register(Participant)
admin.site.register(Trial, TrialAdmin)
