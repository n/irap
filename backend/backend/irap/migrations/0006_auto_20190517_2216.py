# Generated by Django 2.1.7 on 2019-05-17 22:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('irap', '0005_auto_20190515_0454'),
    ]

    operations = [
        migrations.AddField(
            model_name='study',
            name='intertrial_interval',
            field=models.IntegerField(default=400, help_text='milliseconds'),
        ),
        migrations.AddField(
            model_name='study',
            name='max_training_latency',
            field=models.IntegerField(default=2000, help_text='milliseconds'),
        ),
        migrations.AddField(
            model_name='study',
            name='min_training_accuracy',
            field=models.IntegerField(default=80, help_text='percent'),
        ),
        migrations.AddField(
            model_name='study',
            name='num_practice_blocks',
            field=models.IntegerField(default=4),
        ),
        migrations.AddField(
            model_name='study',
            name='num_test_blocks_pairs',
            field=models.IntegerField(default=3),
        ),
        migrations.AddField(
            model_name='study',
            name='training_latency_statistic',
            field=models.CharField(choices=[('MEDIAN', 'median'), ('MEAN', 'Mean')], default='MEDIAN', max_length=6),
        ),
    ]
