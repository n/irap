# Generated by Django 2.1.7 on 2021-02-19 14:59

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('irap', '0014_auto_20190613_1130'),
    ]

    operations = [
        migrations.CreateModel(
            name='TestTrial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('participant', models.CharField(max_length=255)),
                ('block', models.IntegerField()),
                ('accuracy', models.IntegerField()),
                ('speed', models.IntegerField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'test_trial',
                'verbose_name_plural': 'test_trials',
                'ordering': ('-created_at',),
            },
        ),
        migrations.AlterModelOptions(
            name='trial',
            options={'ordering': ('-created_at',), 'verbose_name': 'trial', 'verbose_name_plural': 'trials'},
        ),
        migrations.AddField(
            model_name='trial',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='study',
            name='intertrial_interval',
            field=models.IntegerField(default=400, help_text='milliseconds!'),
        ),
        migrations.AlterField(
            model_name='study',
            name='num_block_pairs',
            field=models.IntegerField(default=2),
        ),
    ]
