from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Study(models.Model):
    '''represents one study'''
    # name of study
    name = models.CharField(max_length=50)
    # i.e. you like barcelona and dislike chelsea
    rule_a = models.CharField(max_length=300)
    rule_b = models.CharField(max_length=300)

    pre_session_instructions = models.TextField(default="This task will present sets of words or images."
                                                " You will be asked to relate the words or images."
                                                " If you make a mistake you'll see a red X."
                                                " Provide the correct response to continue."
                                                " Respond as accurately as you can."
                                                " When you've learrned to be accurate you'll naturally speed up too.")
    pre_block_rules = models.TextField(default="Press space to continue. "
                                       "The previously correct and incorrect answers have now been reversed.")

    # need to add statistics i.e. goal, accuracy, speed
    post_block_feedback = models.TextField(default="Continue responding both as accurately and as quickly as you can.")

    end_feedback = models.TextField(default="Saving Data")

    # labels above responses
    response_1_text = models.CharField(max_length=300, default="Press 'd' for")
    response_2_text = models.CharField(max_length=300, default="Press 'k' for")

    max_num_practice_block_pairs = models.IntegerField(default=4)
    num_block_pairs = models.IntegerField(default=2)

    min_training_accuracy = models.IntegerField(default=80, help_text="percent")
    max_training_latency = models.IntegerField(default=2000, help_text="milliseconds")

    MEDIAN = "MEDIAN"
    MEAN = "MEAN"
    LATENCY_STATISTIC_CHOICES = [(MEDIAN, "median"), (MEAN, "mean")]

    latency_statistic = models.CharField(choices=LATENCY_STATISTIC_CHOICES,
                                                  default=MEDIAN,
                                                  max_length=6)

    intertrial_interval = models.IntegerField(default=400, help_text="milliseconds!")

    def __str__(self):
        return self.name



class Label(models.Model):
    '''i.e. chelsea or barceloa'''
    study = models.ForeignKey(Study, on_delete=models.CASCADE)
    text = models.CharField(max_length=100)
    is_first_group = models.BooleanField()

    def __str__(self):
        return self.text


# need to differentiate between target 1 and target 2
class Target(models.Model):
    '''great, amazing, brilliant'''
    study = models.ForeignKey(Study, on_delete=models.CASCADE)
    text = models.CharField(max_length=100)
    is_first_group = models.BooleanField()

    def __str__(self):
        return self.text


class Response(models.Model):
    '''agree diagree'''
    study = models.ForeignKey(Study, on_delete=models.CASCADE)
    text = models.CharField(max_length=100, help_text="ideally a verb")
    is_first_group = models.BooleanField()

    def __str__(self):
        return self.text


# saving data
class Participant(models.Model):
    '''person taking the study'''
    study = models.ForeignKey(Study, on_delete=models.CASCADE)
    # note = models.TextField()

    # MALE = "MALE"
    # FEMALE = "FEMALE"
    # OTHER = "OTHER"
    # GENDER_CHOICES = [(MALE, "male"), (FEMALE, "female"), (OTHER, "other")]
    # gender = models.CharField(max_length=20)

    # age = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)


class Trial(models.Model):
    responseID = models.CharField(max_length=20, default="Participant_ID")
#     label = models.ForeignKey(Label, on_delete=models.CASCADE)
#     target = models.ForeignKey(Target, on_delete=models.CASCADE)

    label_id = models.CharField(max_length=3, default="0")
    label_is_first_group = models.BooleanField(default=True)
    label_text = models.CharField(max_length=20, default="label")

    target_id  = models.CharField(max_length=3, default="0")
    target_is_first_group = models.BooleanField(default=True)
    target_text = models.CharField(max_length=20, default="target")

    RULE_A = "a"
    RULE_B = "b"
    RULE_CHOICES = [(RULE_A, "a"), (RULE_B, "b")]
    rule = models.CharField(max_length=1, choices=RULE_CHOICES)


    correct = models.BooleanField()
    time_to_correct = models.IntegerField()
    time_to_incorrect = models.IntegerField(null=True, blank=True)

    # format: label group, target group
    TYPE_1 = 1 # 1,1
    TYPE_2 = 2 # 1,2
    TYPE_3 = 3 # 2,1
    TYPE_4 = 4 # 2,2
    TYPE_CHOICES = [(TYPE_1, 1), (TYPE_2, 2), (TYPE_3, 3), (TYPE_4, 4)]
    trial_type = models.IntegerField(choices=TYPE_CHOICES)

    is_test_block = models.BooleanField()

    def __str__(self):
        return str(self.responseID)

#     class Meta:
#         verbose_name = _('trial')
#         verbose_name_plural = _('trials')




