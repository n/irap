from graphene import Boolean, Field, ID, InputObjectType, Mutation, String, Int
from .models import Trial

# class TrialSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = TestTrial
#         fields = {
#             'id',
#             'participant',
#             'block',
#             'accuracy',
#             'speed',
#         }

# class TrialMutation(Mutation):
#     '''record a new trial'''
#     class Arguments:
#         participant = ID(required=True)
#         label = ID(required=True)
#         target = ID(required=True)
#         rule = String(required=True)
#         correct = Boolean(required=True)
#         time_to_correct = Int(required=True)
#         time_to_incorrect = Int(required=True)
#         trial_type = Int(required=True)
#         is_test_block = Boolean(required=True)
#
#     def mutate(self, info, participant, label, target, rule, correct, time_to_correct, time_to_incorrect, trial_type, is_test_block):
#         trial = Trial()
#         trial.participant = Participant.objects.get(pk=1)
#         ###########################
