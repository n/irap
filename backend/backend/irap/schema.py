import graphene
from graphene_django.types import DjangoObjectType, ObjectType
from .models import Study, Label, Target, Response, Participant, Trial

class StudyType(DjangoObjectType):
    class Meta:
        model = Study

class LabelType(DjangoObjectType):
    class Meta:
        model = Label

class TargetType(DjangoObjectType):
    class Meta:
        model = Target

class ResponseType(DjangoObjectType):
    class Meta:
        model = Response


class TrialType(DjangoObjectType):
    class Meta:
        model = Trial

class LabelInputType(graphene.InputObjectType):
    '''i.e. chelsea or barceloa'''
#     typename = graphene.String(required=True)
    id = graphene.String(required=True)
    text = graphene.String(required=True)
    is_first_group = graphene.Boolean(required=True)

class TargetInputType(graphene.InputObjectType):
    '''great, amazing, brilliant'''
#     typename = graphene.String(required=True)
    id = graphene.String(required=True)
    text = graphene.String(required=True)
    is_first_group = graphene.Boolean(required=True)


class TrialInput(graphene.InputObjectType):
    responseID = graphene.String(required=True)
#     label_id = graphene.String()
#     label_is_first_group = graphene.Boolean()
#     label_text = graphene.String()
#     target_id = graphene.String()
#     target_is_first_group = graphene.Boolean()
#     target_text = graphene.String()
    label = TargetInputType(required=True)
    target = LabelInputType(required=True)
    rule = graphene.String(required=True)
    correct = graphene.Boolean(required=True)
    time_to_correct_response = graphene.Int(required=True)
    time_to_incorrect_response = graphene.Int()
    trialNumber = graphene.Int()
    trial_type = graphene.Int(required=True)
    is_test_block = graphene.Boolean(required=True)


class CreateTrial(graphene.Mutation):
    responseID = graphene.String()
    label_id = graphene.String(required=True)
    label_is_first_group = graphene.Boolean(required=True)
    label_text = graphene.String(required=True)
#     label = LabelInputType()
#     target = TargetInputType()
    target_id = graphene.String(required=True)
    target_is_first_group = graphene.Boolean(required=True)
    target_text = graphene.String(required=True)
    rule = graphene.String()
    correct = graphene.Boolean()
    time_to_correct = graphene.Int()
    time_to_incorrect = graphene.Int()
    trial_type = graphene.Int()
    is_test_block = graphene.Boolean()

    class Arguments:
#         input = TrialInput(required=True)
        responseID = graphene.String(required=True)
#         label_id = graphene.String(required=True)
#         label_is_first_group = graphene.Boolean(required=True)
#         label_text = graphene.String(required=True)
#         target_id = graphene.String(required=True)
#         target_is_first_group = graphene.Boolean(required=True)
#         target_text = graphene.String(required=True)
        label = LabelInputType(required=True)
        target = TargetInputType(required=True)
        rule = graphene.String(required=True)
        correct = graphene.Boolean(required=True)
        time_to_correct = graphene.Int(required=True)
        time_to_incorrect = graphene.Int()
        trial_type = graphene.Int(required=True)
        is_test_block = graphene.Boolean(required=True)

    output = None

    def mutate(self, info, responseID, label, target, rule, correct, time_to_correct, time_to_incorrect, trial_type, is_test_block):
        trial = Trial.objects.create(responseID = responseID,
                      label_id = label.id,
                      label_is_first_group = label.is_first_group,
                      label_text = label.text,
                      target_id = target.id,
                      target_is_first_group = target.is_first_group,
                      target_text = target.text,
                      rule=rule,
                      correct=correct,
                      time_to_correct=time_to_correct,
                      time_to_incorrect=time_to_incorrect,
                      trial_type=trial_type,
                      is_test_block=is_test_block)

        return CreateTrial(
            responseID = trial.responseID,
            label_id = trial.label_id,
            label_is_first_group = trial.label_is_first_group,
            label_text = trial.label_text,
            target_id = trial.target_id,
            target_is_first_group = trial.target_is_first_group,
            target_text = trial.target_text,
            rule = trial.rule,
            correct = trial.correct,
            time_to_correct = trial.time_to_correct,
            time_to_incorrect = trial.time_to_incorrect,
            trial_type = trial.trial_type,
            is_test_block = trial.is_test_block,
        )


class CreateTrials(graphene.Mutation):
    completedTrials = graphene.List(TrialInput)

    class Arguments:
        completedTrials = graphene.List(graphene.NonNull(TrialInput), required = True)

    class Meta:
        output = graphene.List(TrialType)
#     Output = graphene.List(TrialType)

    def mutate(self, info, completedTrials):
        new_trials = []
        for trial in completedTrials:
            new_trial = Trial.objects.create(responseID=trial.responseID,
                          label_id = trial.label.id,
                          label_text = trial.label.text,
                          label_is_first_group = trial.label.is_first_group,
                          target_id=trial.target.id,
                          target_text=trial.target.text,
                          target_is_first_group=trial.target.is_first_group,
                          rule=trial.rule,
                          correct=trial.correct,
                          time_to_correct=trial.time_to_correct_response,
                          time_to_incorrect=trial.time_to_incorrect_response,
                          trial_type=trial.trial_type,
                          is_test_block=trial.is_test_block)
#             trial.save()
            new_trials.append(new_trial)
        return new_trials

#         return CreateTrials(
#             Output = new_trials
#         )

class TrialMutation(graphene.ObjectType):
    create_trial = CreateTrial.Field()
    create_trials = CreateTrials.Field()

# class TrialMutation(graphene.Mutation):
#     '''record a new trial'''
#     class Arguments:
#         responseID = graphene.String(required=True)
#         label = LabelInputType(required=True)
#         target = TargetInputType(required=True)
#         rule = graphene.String(required=True)
#         correct = graphene.Boolean(required=True)
#         time_to_correct = graphene.Int(required=True)
#         time_to_incorrect = graphene.Int(required=True)
#         trial_type = graphene.Int(required=True)
#         is_test_block = graphene.Boolean(required=True)
#
#     trial = graphene.Field(lambda: Trial)
#
#     def mutate(root, info, responseID, label, target, rule, correct, time_to_correct, time_to_incorrect, trial_type, is_test_block):
#         trial = Trial(responseID=responseID,
#                       label=label,
#                       target=target,
#                       rule=rule,
#                       correct=correct,
#                       time_to_correct=time_to_correct,
#                       time_to_incorrect=time_to_incorrect,
#                       trial_type=trial_type,
#                       is_test_block=is_test_block)
#         return TrialMutation(trial = trial)
# #         trial.participant = Participant.objects.get(pk=1)
#         ###########################
#
# # class GetTrials(graphene.Mutation):
# #     class Arguments:
# #         trials = graphene.List(Trial)
# #
# #     output = graphene.List(Trial)
#
# #     @staticmethod
# #     def mutation(root, info, trials):
# #         trails =


class Query(ObjectType):
    study = graphene.Field(StudyType, id=graphene.Int(), name=graphene.String())
    label = graphene.Field(LabelType, id=graphene.Int())
    target = graphene.Field(TargetType, id=graphene.Int())
    response = graphene.Field(ResponseType, id=graphene.Int())

    studies = graphene.List(StudyType)
    labels = graphene.List(LabelType)
    targets = graphene.List(TargetType)
    responses = graphene.List(ResponseType)


    # resolver for any single object
    def resolve_singular(self, info, objtype, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            return objtype.objects.get(pk=id)
        return None


    def resolve_study(self, info, **kwargs):
        id = kwargs.get('id')
        name = kwargs.get('name')

        if id is not None:
            return Study.objects.get(pk=id)

        if name is not None:
            return Study.objects.get(name=name)

        return None

    def resolve_label(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Label.objects.get(pk=id)
        return None

    def resolve_target(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Target.objects.get(pk=id)
        return None


    def resolve_response(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Response.objects.get(pk=id)
        return None





    def resolve_studies(self, info, **kwargs):
        return Study.objects.all()

    def resolve_labels(self, info, **kwargs):
        return Label.objects.all()

