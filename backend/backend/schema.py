from graphene import Schema, ObjectType
import backend.irap.schema
from backend.irap.schema import Query, TrialMutation
from backend.irap.models import Trial
# from backend.irap.mutations import TestTrialCreate

class Query(Query, ObjectType):
    pass

# class Mutation(ObjectType):
#     test_trial_create = TestTrialCreate.Field()
class Mutation(TrialMutation, ObjectType):
    pass

schema = Schema(query=Query, mutation=Mutation)
