rsync -av --exclude '.git*' --exclude 'deploy.sh' --exclude '*venv' --exclude '*db.sqlite3' --exclude '.htaccess' \
	  backend/ ~/django-root/

rsync -av www-root/static ~/www-root

cd frontend/ || exit
npm run-script build
cd ../
rsync -av frontend/dist/ ~/www-root/app
rsync -av frontend/assets ~/www-root/app/



# replaces <USER_NAME_HERE> with the current user.
# this only works if deploying from the silk server directly,
# otherwise, go to frontend/src/index.js and replace <USER_NAME_HERE> with
# the user you are deploying to
sed -i "s/<USER_NAME_HERE>/${USER}/" ~/www-root/app/main.js
