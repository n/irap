import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";



import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";



import Trial from "./Trial.jsx";

export default class Block extends React.Component {
    constructor(props) {
        super(props);

        let trialSet = [];
        // build a list of all combinations of labels and targets
        for (let i = 0; i < props.labelSet.length; i++) {
            for (let j = 0; j < props.targetSet.length; j++) {
                const label = props.labelSet[i];
                const labelClean = {id:label.id, text:label.text, isFirstGroup: label.isFirstGroup};
                const target = props.targetSet[j];
                const targetClean = {id:target.id, text:target.text, isFirstGroup: target.isFirstGroup};
                trialSet.push({responseID: this.props.ResponseID,
                               label: labelClean,
                               target: targetClean,
                               trialType: getTrialType(label.isFirstGroup, target.isFirstGroup),
                              });
            }
        }
        // suffle them
        shuffleArray(trialSet);
        this.state = {trialSet: trialSet,
                      completedTrials: [],
                      currentTrialIndex: 0,
                      inTimeout: false,
                      answered: false};

        this.saveTrial = this.saveTrial.bind(this);
        this.gotoNextTrial = this.gotoNextTrial.bind(this);
        this.answeredWrong = this.answeredWrong.bind(this);

    }

    answeredWrong() {
        this.setState({ answered: true });
    }



    saveTrial (response) {
        let completedTrials = this.state.completedTrials;
        completedTrials.push({...this.state.trialSet[this.state.currentTrialIndex],
                              ...response,
                              trialNumber: this.state.currenTrialIndex});
        this.setState({completedTrials: completedTrials,
                       answered: false});
        this.gotoNextTrial();
    }

    gotoNextTrial() {
        const finalTrialIndex = this.state.trialSet.length - 1;
        const currentTrialIndex = this.state.currentTrialIndex;

        if (currentTrialIndex === finalTrialIndex) {
            // go to next block
            this.props.gotoNextBlock(this.state.completedTrials);
        } else {
            this.setState({inTimeout: true});
            setTimeout( function() {this.setState({currentTrialIndex: currentTrialIndex + 1,
                                                   inTimeout: false});}.bind(this), this.props.intertrialInterval);
        }
    }




    render() {

        const currentTrial = this.state.trialSet[this.state.currentTrialIndex];

        let component;
        let incorrectIndicator;


        const correctResponse=[1, 4].includes(currentTrial.trialType)  ===
              (this.props.currentRule === "a");

        if (this.state.inTimeout) {
            component = null;
            incorrectIndicator = null;
        } else {
            component = <Trial ResponseID={this.props.ResponseID}
                               {...currentTrial}
                               currentRule={this.props.currentRule}
                               responseSet={this.props.responseSet}
            // 1 and 4 are the two trial types which have matching label
            // and target groups. See getTrialType function below
                               correctResponse={correctResponse}
                               saveTrial={this.saveTrial}
                               answeredWrong={this.answeredWrong}/>;
            incorrectIndicator = this.state.answered ? <h1 style={{color:"red"}} className="display-4">X</h1> : null;
        }

        return (
            <Container className="text-center">
              <Row style={{height: "150px"}}>
                <Col>
                  {component}
                </Col>
              </Row>

              <Row>
                <Col>
                  <p>Press <strong>'d'</strong> for</p>
                  <h3>{this.props.responseSet[0].text}</h3>
                </Col>
                <Col>
                  {incorrectIndicator}
                </Col>
                <Col>
                  <p>Press <strong>'k'</strong> for</p>
                  <h3>{this.props.responseSet[1].text}</h3>
                </Col>
              </Row>

              {/*<p>{correctResponse ? "d" : "k"}</p>*/}


            </Container>
        );
    }

}


// https://stackoverflow.com/a/12646864
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

function getTrialType (labelGroup, targetGroup) {
    if (labelGroup && targetGroup) {         // 1,1 if rule a, correct: 1
        return 1;
    } else if (labelGroup && !targetGroup) { // 1,2 if rule a, correct: 2
        return 2;
    } else if (!labelGroup && targetGroup) { // 2,1 if rule a, correct: 2
        return 3;
    } else {                                 // 2,2 if rule a, correct: 1
        return 4;
    }
}
