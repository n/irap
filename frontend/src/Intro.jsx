import React, { Component } from 'react';

export default class Intro extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <p>{this.props.preSessionInstructions}</p>
        );
    }
}
