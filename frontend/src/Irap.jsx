import React, { Component, Fragment } from 'react';
// import { Query } from 'react-apollo';

import { Query } from "@apollo/client/react/components";

import gql from 'graphql-tag';

import Study from './Study.jsx';
import {useQuery} from "@apollo/client";

// query to get one study
const GET_STUDY = gql`
query StudyDetail($studyName: String!) {
  study(name: $studyName) {
	name
	ruleA
	ruleB
	preSessionInstructions
	preBlockRules
	postBlockFeedback
	endFeedback
	response1Text
	response2Text
	maxNumPracticeBlockPairs
	numBlockPairs
	minTrainingAccuracy
	maxTrainingLatency
	latencyStatistic
	intertrialInterval
	labelSet {
	  id
	  text
	  isFirstGroup
	}
	targetSet {
	  id
	  text
	  isFirstGroup
	}
	responseSet {
	  id
	  text
	  isFirstGroup
	}
  }
}
`;

export default class Irap extends Component  {
    render () {
        return (
            <Query query={GET_STUDY} variables={{ studyName: this.props.studyName }}>
            {({ data, loading, error}) => {
                if (loading) return <p>Loading...</p>;
                if (error) return <p>Error</p>;

                return (
                    // labelSet targetSet intertrialInterval responseSet
                    <div style={{border:"lightgray solid 2px", height: "500px"}} className="p-3">
                      <Study {...data.study} ResponseID={this.props.ResponseID}/>
                    </div>
                );
            }}
            </Query>
        );
    }
}

// const Irap = () => {
//     console.log("Hello");
//     const {loading, error, data} = useQuery(GET_STUDY, {variables: {studyName: this.props.studyName}});
//     if (loading) return <p>Loading...</p>;
//     if (error) return <p>Error</p>;
//     console.log('data', data);
//     return (
//         // labelSet targetSet intertrialInterval responseSet
//         <div style={{border:"lightgray solid 2px", height: "500px"}} className="p-3">
//             <Study {...data.study} ResponseID={this.props.ResponseID}/>
//         </div>
//     );
// };
//
// export default Irap

//
// export default class Irap extends Component  {
//     render () {
//         return (
//             <Query query={GET_STUDY} variables={{ studyName: this.props.studyName }}>
//             {({ data, loading, error}) => {
//                 if (loading) return <p>Loading...</p>;
//                 if (error) return <p>Error</p>;
//
//                 return (
//                     // labelSet targetSet intertrialInterval responseSet
//                     <div style={{border:"lightgray solid 2px", height: "500px"}} className="p-3">
//                       <Study {...data.study} ResponseID={this.props.ResponseID}/>
//                     </div>
//                 );
//             }}
//             </Query>
//         );
//     }
// }
