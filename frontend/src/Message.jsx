import React, { Fragment } from "react";
import KeyboardEventHandler from 'react-keyboard-event-handler';

export default function Message(props) {
    return(
        <Fragment>
          {props.children}
        <KeyboardEventHandler handleKeys={['space']}
                              onKeyEvent={(key, e) => props.onClick()}
                              handleEventType="keyup"/>
        </Fragment>
    );
}
