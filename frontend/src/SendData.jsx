import React from "react";
// import {useMutation} from "react-apollo"
import gql from 'graphql-tag';
import {ApolloProvider, ApolloClient, HttpLink, InMemoryCache, useMutation} from "@apollo/client";
import { Mutation } from "@apollo/client/react/components";

const CREATE_TRIALS = gql`
mutation CreateTrials($completedTrials: [TrialInput!]!) {
  createTrials(completedTrials: $completedTrials){
   responseID
  }
}
`;

const SendData = (completedTrials) => {
    const [createTrials, {data}] = useMutation(CREATE_TRIALS);
    return(
        <div style={{display: 'flex', width: '100%', justifyContent: 'center', alignItems: 'center'}}>
            <button onClick={
                e => {
                    e.preventDefault();
                    createTrials({variables: {completedTrials: completedTrials.completedTrials}})
                        .then();
                }
            }>
                Submit
            </button>
        </div>

    )
};

export default SendData;
