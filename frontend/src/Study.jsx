import React, { Component, Fragment } from 'react';

import Message from "./Message.jsx";
import Block from "./Block.jsx";
import ProgressBar from "react-bootstrap/ProgressBar";

import * as math from 'mathjs';


import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Table from "react-bootstrap/Table";

import "./custom.css";
import SendData from "./SendData.jsx"


export default class Study extends Component {

    constructor(props) {
        super(props);



        const isTestBlock = props.maxNumPracticeBlockPairs > 0;
        const initialRule = Math.random() >= 0.5 ? "a" : "b";
        const totalBlocks = props.maxNumPracticeBlockPairs * 2;

        this.state = {ResponseID: this.props.ResponseID,
                      initialRule: initialRule,
                      currentRule: initialRule,
                      screen: "instructions",
                      isTestBlock: isTestBlock,
                      currentBlockPair: 0,
                      completedTrials: [],
                      previousBlockAccuracy: 0,
                      previousBlockLatency: 0,
                      totalBlocks: totalBlocks,
                     };
        this.gotoNextBlock = this.gotoNextBlock.bind(this);
    }

    gotoNextBlock(trials) {
        let completedTrials = this.state.completedTrials;
        let newTrials = trials.map(trial => ({...trial,
                                              isTestBlock: this.state.isTestBlock,
                                              rule: this.state.currentRule}));
        completedTrials.push(...newTrials);


        const previousBlockAccuracy = this.getAccuracy(trials);
        const previousBlockLatency = this.getLatency(trials);


        const newRule = this.state.currentRule === "a" ? "b" : "a";
        let nextScreen = "feedback";
        let resetBlockCount = false;
        let continueToMain = false;

        // if we're done with this block pair
        if (this.state.currentRule !== this.state.initialRule) {

            // if we're in the practice phase
            if (this.state.isTestBlock) {
                // get previous block's accuracy and latency

                // number of trials in each block
                const trialsPerBlock = this.props.labelSet.length * this.props.targetSet.length;
                // get the last two blocks worth of trials
                const blockPairTrials = completedTrials.slice(
                    completedTrials.length - trialsPerBlock * 2, completedTrials.length
                );

                const firstBlockOfPair = blockPairTrials.slice(0, trialsPerBlock);
                const secondBlockOfPair = blockPairTrials.slice(trialsPerBlock,completedTrials.length);

                const blockPairAccuracy = this.getAccuracy(blockPairTrials);
                const blockPairLatency = this.getLatency(blockPairTrials);

                const firstBlockAccuracy = this.getAccuracy(firstBlockOfPair);
                const firstBlockLatency = this.getLatency(firstBlockOfPair);

                const secondBlockAccuracy = this.getAccuracy(secondBlockOfPair);
                const secondBlockLatency = this.getLatency(secondBlockOfPair);

                const firstBlockPass = firstBlockAccuracy >= this.props.minTrainingAccuracy / 100 &&
                                        firstBlockLatency <= this.props.maxTrainingLatency;
                const secondBlockPass = secondBlockAccuracy >= this.props.minTrainingAccuracy / 100 &&
                    secondBlockLatency <= this.props.maxTrainingLatency;

                // if we passed
                if(firstBlockPass && secondBlockPass){
                    // go on to the main blocks
                    console.log("successfully completed training");
                    this.setState({totalBlocks: this.props.numBlockPairs * 2});
                    nextScreen = "feedbackThenPostTraining";
                    resetBlockCount = true;
                    continueToMain = true;

                } else if (this.props.maxNumPracticeBlockPairs * 2 === this.state.currentBlockPair + 1){ // check if we've maxed out on practice blocks
                    // end the study
                    console.log("failed training");
                    nextScreen = "endFailedTraining";
                } else { // moving on to next block pair of training
                    nextScreen = "feedback";
                    resetBlockCount = false;
                }


            } else { // if we're in the normal block phase

                // if we're all done with study
                if ( this.state.currentBlockPair + 1 >= this.props.numBlockPairs * 2) {
                    nextScreen = "feedbackThenEnd";
                } else {
                    nextScreen = "feedback";
                    resetBlockCount = false;
                }

            }

        } else {
            // still on current block pair
            console.log("still on current block pair");
            nextScreen = "feedback";
        }

        // if we're in test block and say we want to switch, then switch. otherwise stay the same
        const isTestBlock = this.state.isTestBlock && continueToMain ? false : this.state.isTestBlock;

        // changed the state based on everything above
        this.setState((state, props) => ({ currentRule: newRule,
                                           completedTrials: continueToMain ? [] : completedTrials,
                                           previousBlockAccuracy: previousBlockAccuracy,
                                           previousBlockLatency: previousBlockLatency,
                                           screen: nextScreen,
                                           currentBlockPair: resetBlockCount ? 0 : state.currentBlockPair + 1,
                                           isTestBlock: isTestBlock }));

    }


    getLatency(trials) {
        let responseTime;

        // find either the mean or median of the trial times
        if (this.props.latencyStatistic === "MEAN") {
            responseTime = math.mean(trials.map(trial => trial.timeToCorrectResponse));
        } else {
            responseTime = math.median(trials.map(trial => trial.timeToCorrectResponse));
        }

        return responseTime;
    }

    getAccuracy(trials) {
        return math.mean(trials.map(trial => trial.correct));
    }

    feedbackTable () {
        return(
            <Table striped bordered hover>
              <thead>
                <tr>
                  <td></td>
                  <td>Your scores on the previous block</td>
                  <td>Goal scores</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Accuracy</td>
                  <td>{Math.round(this.state.previousBlockAccuracy * 100)} %</td>
                  <td>&gt; {Math.round(this.props.minTrainingAccuracy)} %</td>
                </tr>
                <tr>
                  <td>Speed</td>
                  <td>{Math.round(this.state.previousBlockLatency)} milliseconds</td>
                  <td>&lt; {Math.round(this.props.maxTrainingLatency)} milliseconds</td>
                </tr>
              </tbody>
            </Table>
        );
    }


    render() {


        switch(this.state.screen) {
        case "instructions":
            return (
                <Message onClick={() => this.setState({ screen: "preTrainingInfo" })}>
                  <p className="text-left">{this.props.preSessionInstructions}</p>
                  <p>Press <strong>space</strong> to continue.</p>
                </Message>
            );
            break;

        case "preTrainingInfo":
            return (
                <Message onClick={() => this.setState({ screen: "rules" })}>
                  <p className="text-left">
                    You will now begin the training portion of this task.
                    The purpose of this is to familiarize yourself with the format of the
                    task, and develop proficiency at responding quickly and accurately.
                  </p>
                  <p className="text-left">
                    Place a left finger on the <strong>'d'</strong> key,
                    and a right finger on the <strong>'k'</strong>,
                    and your thumbs on the <strong>spacebar</strong>.
                  </p>
                  <p className="text-left">
                    On the following screen, you will be presented with a rule which you
                    will treat as the "truth" for the first set of questions.
                    When, according to the given rule, the two words or images are <strong>{this.props.responseSet[0].text.toLowerCase()}</strong>,  press the <strong>'d'</strong> key.
                    When they are <strong>{this.props.responseSet[1].text.toLowerCase()}</strong>,  press the <strong>'k'</strong> key.
                  </p>
                  <p className="text-left">
                    Be sure to press and release the keys, not hold them down.
                  </p>
                  <p>Press <strong>space</strong> to continue.</p>
                </Message>

            );
            break;

        case "rules":
            return (
                <Message onClick={() => this.setState({ screen: "block" })}>
                  <p>Respond according to the following statement:</p>
                  <p>{this.state.currentRule === "a"
                      ? this.props.ruleA
                      : this.props.ruleB}</p>
                  <p>Press <strong>space</strong> to begin.</p>
                </Message>
            );
            break;

        case "feedback":
            return(
                <Fragment>
                    <Message onClick={() => this.setState({ screen: "rules" })}>
                        {this.feedbackTable()}
                        <p>Press <strong>space</strong> to continue.</p>
                    </Message>
                </Fragment>
            );
            break;

        case "feedbackThenPostTraining":
            return(
                <Message onClick={() => this.setState({ screen: "postTraining" })}>
                  {this.feedbackTable()}
                  <p>Press <strong>space</strong> to continue.</p>
                </Message>
            );
            break;

        case "postTraining":
            return(
                <Message onClick={() => this.setState({ screen: "rules" })}>
                  <p>You will now begin the main phase of the task.</p>
                  <p>Press <strong>space</strong> to continue.</p>
                </Message>
            );
            break;

        case "feedbackThenEnd":
            return(
                <Message onClick={() => this.setState({ screen: "end" })}>
                  {this.feedbackTable()}
                  <p>Press <strong>space</strong> to continue.</p>
                </Message>
            );
            break;

        case "endFailedTraining":
            return(
                <p>
                  You were unable to reach the standards required for taking the test,
                  and the task is now over. Thank you for your participation.
                </p>
            );
            break;

        case "end":
            return(
                <Fragment>
                    <p>{this.props.endFeedback}</p>
                    <SendData completedTrials={this.state.completedTrials}/>
                </Fragment>
            );
            break;

        case "block":
            return(
                <Fragment>
                    <p className="text-right small">
                        {/*({this.state.isTestBlock ? "Test" : "Main"} block {(this.state.currentBlockPair * 2) + (this.state.currentRule !== this.state.initialRule ? 1 : 0) + 1} of {this.state.isTestBlock ? this.props.maxNumPracticeBlockPairs * 2 : this.props.numBlockPairs * 2})*/}
                        {/*({this.state.isTestBlock ? "Test" : "Main"} block {this.state.currentBlockPair + 1} of {this.state.isTestBlock ? this.props.maxNumPracticeBlockPairs * 2 : this.props.numBlockPairs * 2})*/}

                    </p>
                    <Block ResponseID={this.props.ResponseID}
                           currentRule={this.state.currentRule}
                           labelSet={this.props.labelSet}
                           targetSet={this.props.targetSet}
                           responseSet={this.props.responseSet}
                           intertrialInterval={this.props.intertrialInterval}
                           gotoNextBlock={this.gotoNextBlock}/>
                    <ProgressBar now ={(this.state.currentBlockPair + 1) * 100 / this.state.totalBlocks} label = {`${this.state.currentBlockPair + 1} of ${this.state.totalBlocks} ${this.state.isTestBlock ? " (Maximum)": ""}`} style={{marginTop: '190px'}}/>
                </Fragment>
            );
            break;

        default:
            console.error(this.state.screen + " is not a valid screen");
            break;
        }
    }
}


