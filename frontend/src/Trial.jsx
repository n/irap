import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import KeyboardEventHandler from 'react-keyboard-event-handler';


function getLabel(label) {
    // if the label is an image:
    if (label.endsWith('.png') || label.endsWith('.jpg')) {
        return <img 
			src={"/app/assets/" + label}
			alt={label}
			style={{maxHeight:"300px", width:"auto"}}
		/> // show image
    }

    // else, show the text label:
    return <h2>{label}</h2>;
}


export default class Trial extends React.Component {
    constructor(props) {
        super(props);

        this.state = {startTime: Date.now(),
                      answered: false,
                      submitted: false,
                      timeToIncorrectResponse: null};

        this.handleResponse = this.handleResponse.bind(this);

    }



    handleResponse (key) {
        let response = key === 'd' ? this.props.responseSet[0] : this.props.responseSet[1];

        const isCorrect = response.isFirstGroup === this.props.correctResponse;
        const isFirst = !this.state.answered;

        const duration = Date.now() - this.state.startTime;
        const submitted = this.state.submitted;

        // if our response matches the correct response and its the first response
        if (isCorrect && isFirst) {
            this.setState({submitted: true});
            if (!submitted) {
                this.props.saveTrial({correct: true,
                                      timeToIncorrectResponse: null,
                                      timeToCorrectResponse: duration,});
            }

        } else if (!isCorrect && isFirst) { // incorrect first response
            this.setState({answered: true,
                           timeToIncorrectResponse: duration});
            this.props.answeredWrong();

        } else if (isCorrect && !isFirst) { // we already got the answer wrong and now we're right
            this.setState({submitted: true});
            if (!submitted) {
                this.props.saveTrial({correct:false,
                                      timeToIncorrectResponse: this.state.timeToIncorrectResponse,
                                      timeToCorrectResponse: duration});
            }

        } else { // guessed wrong again
            // don't think we need to do anything here
        }

    }

    render () {

        return (
            <Fragment>
            {getLabel(this.props.label.text)}
              <h2>{this.props.target.text}</h2>

            <KeyboardEventHandler
              handleKeys={['d', 'k']}
              onKeyEvent={(key, e) => this.handleResponse(key)}
            />
            </Fragment>
        );
    }
}


function randomElement(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

