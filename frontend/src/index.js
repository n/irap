import React, { Component } from "react";
import ReactDOM from "react-dom";

import Container from "react-bootstrap/Container";

import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";


import css from'bootstrap/dist/css/bootstrap.min.css';


// apollo imports
// import { ApolloProvider } from 'react-apollo';
// import { ApolloClient } from "apollo-client";
// import { InMemoryCache } from "apollo-cache-inmemory";
// import { HttpLink } from "apollo-link-http";
import { ApolloProvider, ApolloClient, InMemoryCache, HttpLink } from "@apollo/client"

import Irap from "./Irap.jsx";
import {useMutation} from "@apollo/client";


const cache = new InMemoryCache();
const link = new HttpLink({
      //uri: "http://127.0.0.1:8000/backend/graphql/"
	uri: "https://<USER_NAME_HERE>.w3.uvm.edu/backend/graphql/"
});
const client = new ApolloClient({
    cache, link
});


function Home() {
	return <div>
            <h1>Homepage</h1>
            <Link to={"ff"}>link to ff</Link>
            <Link to={"foobar"}>link to foobar</Link>
            <Link to={"study/SmokingTest/R_2tlN7AJPXVwm7ed"}>link to smoking study</Link>
	</div>;
}

function ff() {
	return <h1>ff - test</h1>;
}

function foobar() {
    return <h1>foobar</h1>;
}

function IrapStudy(props) {
    return(
        <Container className="mt-5">
          {/*<ApolloProvider client={client}>*/}
            <Irap studyName={props.match.params.studyName} ResponseID={props.match.params.RID}/>
          {/*</ApolloProvider>*/}
        </Container>
    );
}

function App() {
	return (
		<Router basename="/app">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/ff" component={ff}/>
              <Route path="/foobar" component={foobar}/>
              <Route path="/study/:studyName/:RID" component={IrapStudy} />
            </Switch>
		</Router>
	);
}


ReactDOM.render(
    <ApolloProvider client={client}>
        <App/>
    </ApolloProvider>
    , document.getElementById("app"));
